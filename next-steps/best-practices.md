# Best practices

It is essential when creating projects to always ensure that you are following best practices and correct naming conventions to keep things uniform and help other people who may work on the project in future.&#x20;

* [Identifier naming rules and conventions](https://learn.microsoft.com/en-us/dotnet/csharp/fundamentals/coding-style/identifier-names)
* [Coding Conventions](https://learn.microsoft.com/en-us/dotnet/csharp/fundamentals/coding-style/coding-conventions)
