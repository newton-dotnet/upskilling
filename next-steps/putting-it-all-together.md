# Putting it all together

Now you have the basics of C# follow the brief below to put everything you have learnt into practice.



We are an e-commerce company primarily selling clothing, we would like some new API’s to perform data operations.&#x20;


The API’s should be written in C# using;&#x20;

* Latest LTS version of .NET (an even number)
* Entity framework
* SQL for the database


The data we would like to track to begin with is listed below, we would like full CRUD functionality for the data. The models can be adapted as you see fit as long as we are still tracking all of the fields contained below. Special attention should be paid to ensure that the Microsoft standards are being followed at all times, as well as ensuring that the application can be modified and extended moving forwards, along with as much separation of concerns as you see possible. Use this as an excuse to show off everything you have learnt so far while also asking questions about anything you may not know.


| **Product**   |
| ------------- |
| Id            |
| DateCreated   |
| Title         |
| Description   |
| Price         |
| StockLevel    |
| LastPurchased |


| **Customer**               |
| -------------------------- |
| Id                         |
| DateCreated                |
| Title                      |
| FirstName                  |
| Surname                    |
| Email                      |
| Orders IEnumerable\<Order> |


| **Order**                      |
| ------------------------------ |
| Id                             |
| DateCreated                    |
| ShippedDate                    |
| TotalPrice                     |
| Customer                       |
| Products IEnumerable\<Product> |