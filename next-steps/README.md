# Next steps

## Overview

Now you have learnt the basics of developing C# applications what next, there are some further challenges to develop your knowledge and put into practice what you have learnt so far. &#x20;

## Useful links

* [Exercism](https://exercism.org/tracks/csharp) - A collection of challenges testing multiple concepts and language features for you to experiment and practice with.

## Topics

* [Best practices](best-practices.md)
* [Refactoring](refactoring.md)
* [Putting it all together ](putting-it-all-together.md)
* [Message queues](message-queues.md)
