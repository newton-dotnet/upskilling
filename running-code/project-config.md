# Project config

So you have created a simple dotnet project.

Within the project's folder you see:

* `Program.cs`
* `<projectname>.csproj`
* `obj` and `bin` directories

Let's open the `.csproj` file and take a look.

## .csproj structure

```xml
<Project Sdk="Microsoft.NET.Sdk">

  <PropertyGroup>
    <OutputType>Exe</OutputType>
    <TargetFramework>net6.0</TargetFramework>
    <ImplicitUsings>enable</ImplicitUsings>
    <Nullable>enable</Nullable>
  </PropertyGroup>

</Project>
```

If you're familiar with Node applications, this is roughly equivalent to a `package.json`.

```xml
<Project Sdk="Microsoft.NET.Sdk">
```

We're using the Microsoft .NET software development kit. Other SDKs are availble:

* Microsoft.NET.Sdk.Web
* Microsoft.NET.Sdk.Razor
* Microsoft.NET.Sdk.Desktop
* alternative runtime targets, such as embedded systems 😎

```xml
<OutputType>Exe</OutputType>
```

Our build will output an executable. Other output options are, for example, a library. Libraries are not executable, but they allow our code to be consumed by other applications.

```xml
<TargetFramework>net6.0</TargetFramework>
```

We are targeting dotnet framework version 6. This enables us to use the latest framework and language features.

dotnet is (mostly) backwardly compatible, so if we have the dotnet 6 SDK we can still target the .net 3 framework. We may need to do this to allow our code to be consumed by older applications.

```xml
<ImplicitUsings>enable</ImplicitUsings>
<Nullable>enable</Nullable>
```

We can toggle some build-time features in our C# code. For example:

* `ImplicitUsings` saves us time and effort by assuming basic imports
* `Nullable`, if disabled, will ensure the compiler is strict about avoiding [nullable references](https://hinchman-amanda.medium.com/null-pointer-references-the-billion-dollar-mistake-1e616534d485) - you will find more about that later in the pathway.

## Conclusion

So now you can read the basic structure of a project file.

You understand how the command `dotnet build` consumes this configuration and generates the resulting binaries.

## Further reading (optional)

The complete project file spec is detailed in the [.NET documentation](https://docs.microsoft.com/en-us/dotnet/core/project-sdk/msbuild-props).
