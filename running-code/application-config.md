# Application config

Code rarely runs in isolation.

Runtime configuration is an important part of C# projects.

Whether it is a database connection string or a "development" flag, we often need to modify the behavior of our code based on external configuration.

Follow this exercise to see how we can inject variables into our code at runtime.

## Step 1: Create a new dotnet console project

Create a new folder called `hello_world_config` and create a new dotnet project within.

```bash
mkdir hello_world_config
cd hello_world_config
dotnet new console
```

Open this new project in Visual Studio Code.

```bash
code .
```

You can now run this simple project using `dotnet run`.

## Step 2: Add an App.config

In the root folder create a file called `App.config`

Copy and past the following content into it:

```xml
<?xml version="1.0" encoding="utf-8" ?>  
<configuration>  
  <appSettings>  
    <add key="greeting" value="Hello from App.config"/>
  </appSettings>  
</configuration>
```

## Step 3: Join up your config to your project

Open the `hello_world_config.csproj`. Within the `<PropertyGroup>` section, add the line:

```xml
<AppConfig>App.config</AppConfig>
```

Save these changes and make sure it is all linked up correctly by running `dotnet build`. You should not see any compilation errors.

## Step 4: Consume the configuration

Our configuration file has a setting called "greeting". We need to change our C# code to consume this property.

To do this we need to use a configuration manager library.

In the CLI, run the following command:

```bash
dotnet add package System.Configuration.ConfigurationManager
```

We will use this configuration manger to access the app config.

Next change your `Project.cs` code file to read:

```csharp
using System.Configuration;

Console.WriteLine(ConfigurationManager.AppSettings["greeting"]);
```

Finally execute `dotnet run` and you should see the greeting from the config file written to the command line 🎉

## Conclusion

We have seen how external variables can be consumed in our C# projects and code.

We have also taken our first steps to importing dependencies, which we will cover more about later in the pathway.
