# Running Code

## Overview

Before jumping straight into the code, it is important to understand the execution environment of C#.

The first tutorial will guide you through setting up your development environment. Download the tools here:

1. [Visual Studio Code](https://code.visualstudio.com/)
2. The [C# plugin for Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=ms-dotnettools.csharp)

## What you will learn

* How to create a new dotnet project from the command line
* How to build and execute a project
* How to read an application configuration
* The tools you will be using for your learning pathway

## Topics

1. [💻 Install the tools and create your first dotnet project](first-project.md)
2. [📖 Read more about project configuration](project-config.md)
3. [📖 Inject settings into an application using App.config](application-config.md)
