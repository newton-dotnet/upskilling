# Persistence

## Overview

Data drives all online services. It is crucial that we understand how data is stored and queried.

This module requires some basic knowledge of relational databases, you can read up about SQL [here](https://www.sqltutorial.org/).&#x20;

## What you will learn

* How to use .NET tools to interface with relational databases
* How document data stores differ from relational models

## Topics

* [DBContext](dbcontext.md)
* [Entity Framework and EF Migrations](entity-framework.md)
* [Document data stores with CosmosDB](cosmosdb.md)
