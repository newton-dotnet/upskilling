# Introduction

## C# Learning Pathway

So you want to master C#? Good choice!

This learning pathway assumes basic knowledge of programming concepts. We've stripped out the real basics, so you won't get bored learning how to write a `for` loop 🥱.

For those who are less confident, Microsoft provides an excellent [introduction to programming with C#](https://docs.microsoft.com/en-us/users/dotnet/collections/yz26f8y64n7k07?WT.mc\_id=dotnet-35129-website) which covers the basics.

## Prerequisites

Throughout this course we rely on some basic knowledge of:

* **git** - to clone git repositories. See how to [get started](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html) with git on gitlab.
* docker - to run a container locally, and to build a container for release. For OSX we recommend installing `docker` and `colima` using [homebrew](https://formulae.brew.sh/formula/colima.)
* [**Visual Studio Code**](https://code.visualstudio.com/) to run the interactive examples

## About these modules

Each module will provide the prerequisites for the next. For example, it is important to know how to build and run code before we dive into the complexities of the language.

Topics are covered by a variety of materials, so expect to see:

* 📖 reading material
* 💻 interactive exercises
* 📹 videos
* 🔧 guided project work

If you find yourself stuck or confused, reach out to a mentor or a peer. We learn best when we learn together 💪. Come find us in the #newton-dotnet slack channel.

## Modules

* [Running Code](running-code/)
  * [First project](running-code/first-project.md)
  * [Project config](running-code/project-config.md)
  * [Application config](running-code/application-config.md)



* [C# fundamentals](c-fundamentals/)
  * [Types](c-fundamentals/types.md)
  * [Classes](c-fundamentals/classes.md)
  * [Interfaces](c-fundamentals/interfaces.md)
  * [IEnumerable and LINQ](c-fundamentals/ienumerable-and-linq.md)
  * [Async with Tasks](c-fundamentals/async-with-tasks.md)
  * [Exceptions and error handling](c-fundamentals/exceptions-and-error-handling.md)
  * [Extension methods](c-fundamentals/extension-methods.md)



* [Delivering a project](delivering-a-project/)
  * [Package management](delivering-a-project/package-management.md)
  * [Unit testing](delivering-a-project/unit-testing.md)
  * [Logging and monitoring](delivering-a-project/logging-and-monitoring.md)
  * [Publishing](delivering-a-project/publishing.md)
  * [Containerisation](delivering-a-project/containerisation.md)



* [Persistence](persistence/)
  * [DBContext](persistence/dbcontext.md)
  * [Entity framework](persistence/entity-framework.md)
  * [CosmosDB](persistence/cosmosdb.md)



* [Next steps](next-steps/)
  * [Message queues](next-steps/message-queues.md)
  * [Putting it all together ](next-steps/putting-it-all-together.md)
  * [Refactoring](next-steps/refactoring.md)
  * [Best practices](next-steps/best-practices.md)

