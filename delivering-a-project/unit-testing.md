# Unit testing

## What is a Unit test **?**

In the software development process Unit Tests basically test individual parts ( also called as Unit ) of code (mostly methods) and make it work as expected by programmer. A Unit Test is a code written by any programmer which test small pieces of functionality of big programs. Performing unit tests is always designed to be simple, A "UNIT" in this sense is the smallest component of the large code part that makes sense to test, mainly a method out of many methods of some class. Generally the tests cases are written in the form of functions that will evaluate and determine whether a returned value after performing Unit Test is equals to the value you were expecting when you wrote the function. The main objective in unit testing is to isolate a unit part of code and validate its to correctness and reliable.&#x20;

## Why do we need Unit tests?

One of the most valuable benefits of using Unit Tests for your development is that it may give you positive confidence that your code will work as you have expected it to work in your development process. Unit Tests always give you the certainty that it will lead to a long term development phase because with the help of unit tests you can easily know that your foundation code block is totally dependable on it.

There are few reasons that can give you a basic understanding of why a developer needs to design and write out test cases to make sure major requirements of a module are being validated during testing,&#x20;

* Unit testing can increase confidence and certainty in changing and maintaining code in the development process.
* Unit testing always has the ability to find problems in early stages in the development cycle.
* Codes are more reusable, reliable and clean.
* Development becomes faster.
* Easy to automate.

## Writing your first unit test

Follow this [Pluralsite course](https://www.pluralsight.com/guides/testing-.net-core-apps-with-visual-studio-code) to write your first unit test in vs code.

## Mocking interfaces

