# Package management

Packages

A package is a way of sharing and using reusable code public c# packages primarily come from nuget.org but you can create your own and store them on a private package gallery; `Azure Artifacts` a great tool for this. Between nuget.org and private package galleries that your organisation might establish, you can find tens of thousands of highly useful packages to use in your apps and services.&#x20;

## Adding or removing packages from your project

### Using the Nuget Gallery

* Firstly you will need [NuGet Gallery](https://marketplace.visualstudio.com/items?itemName=patcx.vscode-nuget-gallery) for Visual Studio Code.
* Once this is installed and with your project open you can type type `Nuget gallery` into the command pallet and open up the gallery.
* Then search for the package you would like to add and select it from the list.
* From there you can then choose the version and which project csproj you would like to install it into.
* You will then need to run `dotnet restore` in the command line, or click the restore button that comes up on vs code.
* You can uninstall the package following the same steps but selecting uninstall.

<figure><img src="../.gitbook/assets/nuget.gif" alt=""><figcaption></figcaption></figure>

### Manually adding and removing packages from the csproj file

If you know the exact name and version of the package you would like to use (this can be found on [nuget.org](https://www.nuget.org/)) then you can manually add the packages to your project in the format shown below.

```xml
<ItemGroup>
    <PackageReference Include="Microsoft.EntityFrameworkCore" Version="2.2.4" />
  </ItemGroup>
```

You can also remove them by just removing the package reference line containing the package you no longer need.&#x20;

Once you have added or removed a package just save the file and then either click the restore button that pops up in the bottom right of vs code or type  `dotnet restore` in the command line.

## Useful packages

* [Newtonsoft.Json](https://www.nuget.org/packages/Newtonsoft.Json/)
* [AutoMapper](https://www.nuget.org/packages/AutoMapper)
* [Serilog](https://www.nuget.org/packages/Serilog/2.12.1-dev-01635)
* [Swagger](https://www.nuget.org/packages/Swashbuckle.AspNetCore.Swagger)

## Creating your own packages

Firstly why might you want to create your own packages, you can do this to share models and logic across multiple projects to prevent repeated code as well as standardising things like error handling and authentication. So lets look at creating one;

1. Firstly create a new project with the command `dotnet new classlib -n 'Package Name'` and open this project in vs code.
2. You can then add the logic or classes that you would like to share, try and keep it so the package just performs a single function. This is to prevent unnecessary code being pulled in when you use the package across all of your other projects.
3. You can then push the code up to Azure devops using git (more information regarding this in the [Publishing](publishing.md) section).
4.  You will then need to set up a build pipeline to build and deploy your package to the Azure Artifacts registry (more info regarding build pipelines in the [Publishing](publishing.md) section). Your pipeline will look something like this; &#x20;

    ```yaml
    name: $(majorMinorVersion).$(semanticVersion)

    trigger:
    - main

    pool:
      vmImage: ubuntu-latest

    variables:
      buildConfiguration: 'Release'
      majorMinorVersion: 1.0
      semanticVersion: $[counter(variables['majorMinorVersion'], 0)]

    steps:
    - task: UseDotNet@2
      displayName: 'Use .NET 6 sdk'
      inputs:
        packageType: 'sdk'
        version: '6.0.x'
        includePreviewVersions: true

    - task: DotNetCoreCLI@2
      displayName: 'Build Middleware'
      inputs:
        command: 'build'
        packagesToPack: '**/*.csproj'
        arguments: '--configuration $(buildConfiguration)'

    - task: DotNetCoreCLI@2
      displayName: 'Pack NuGet Package'
      inputs:
        command: 'pack'
        packagesToPack: '**/*.csproj'
        versioningScheme: 'byBuildNumber'

    - task: DotNetCoreCLI@2
      displayName: 'Push NuGet Package'
      inputs:
        command: 'push'
        packagesToPush: '$(Build.ArtifactStagingDirectory)/*.nupkg'
        nuGetFeedType: 'internal'
        publishVstsFeed: '<your feed id>'
    ```

## Accessing a private Nuget feed

1. Add your private feed to your list of sources using the following command; `dotnet nuget add source -n "my-private-source-name" https://<my-private-source-url>`
2. You will need the [azure artifacts credentials provider](https://github.com/Microsoft/artifacts-credprovider), follow the install instructions.&#x20;
3. You will then be able to search for packages by changing the source in the Nuget gallery or by manually adding them.&#x20;
4. Once you have added one you will need to run `dotnet restore --interactive` this will give you a link to be able to login and authenticate with azure.
5. You are then free to reference the classes or logic just like you would if they were located in the same project.