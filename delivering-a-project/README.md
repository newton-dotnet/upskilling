# Delivering a project

## Overview

To be an effective software engineer you need to be confident delivering good, working software.

This module focuses on the skills you need to deliver large projects. This means ensuring the quality of your work with unit testing and monitoring, and understanding how code from your local environment is ultimately packaged and delivered to a production-ready system.

## What you will learn

* How to make use of the large .NET ecosystem of 3rd party libraries
* How to structure and combine multiple projects to create a large solution
* How to test and monitor your software to ensure quality
* How to publish, ship and host your work on production systems

## Topics

* [Package management](package-management.md)
* [Unit Testing](unit-testing.md)
* [Logging and monitoring](logging-and-monitoring.md)
* [Publishing](publishing.md)
* [Containerisation](containerisation.md)
