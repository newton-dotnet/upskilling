# Exceptions and error handling

From the [Microsoft documentation](https://docs.microsoft.com/en-us/dotnet/csharp/fundamentals/exceptions/) on Exceptions:

> The C# language's exception handling features help you deal with any unexpected or exceptional situations that occur when a program is running. Exception handling uses the try, catch, and finally keywords to try actions that may not succeed, to handle failures when you decide that it's reasonable to do so, and to clean up resources afterward.

### Unexpected or Exceptional situations

Consider some code which:

1. Accepts a student's name from the user input
2. Queries an web API to fetch information on all students with that name
3. Displays the list of results in the format `student_name | age | year_group | home_room`

If everything works as expected, this is considered the "happy path". I enter a student's name, and I see a list of results.

Exceptional behaviour could happen if, for example, the network is broken.

Whilst a broken network is clearly unexpected, consider the following:

* What if there are no matches to the student's name?
* What if the user enters nothing?
* What if the data which is returned from the API doesn't match the fields we need?

It is up to both engineers and designers to consider all situations, and how to respond do them. Some situations may be exceptional (data field mismatch), whilst other situations are predictable (no matches to a student's name).

### Recovering from errors

To recover from exceptional situations we can catch errors and ensure our code is in a fit state to continue.

If an exception is thrown and is _not_ caught, it will result in the entire process exiting. This is not ideal.

Examples of recovering from errors include:

* Catching a network error and showing a failure message to the user
* Expecting user input to be a number but receiving a string (e.g. "seven") and showing a validation error on the data capture form
* Writing a file to a disk which has run out of space and informing the user

## Exceptions

**Work through the** [**interactive examples of throwing, catching and extending exceptions**](vscode://ms-dotnettools.dotnet-interactive-vscode/openNotebook?notebookFormat=dib\&url=https://andigital.gitlab.io/hamilton/learning-dotnet/csharp\_fundamentals/exceptions.dib)**.**

## Best practices

**Read and understand the** [**Microsoft recommended best practices**](https://docs.microsoft.com/en-us/dotnet/standard/exceptions/best-practices-for-exceptions) **for exceptions**
