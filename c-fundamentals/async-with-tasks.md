# Async with Tasks

## What is a Thread and why do we need them?

**Watch this explainer on multithreading** to appreciate the low-level complexities that tasks and threads help us to solve.

* 📹 [https://www.youtube.com/watch?v=7ENFeb-J75k](https://www.youtube.com/watch?v=7ENFeb-J75k)

So we've seen how Threads were created to solve the issues around parallel code execution.

## What is a Task?

A Task is an abstraction for .Net (and C#) to help us write asynchronous and concurrent software.

The [Task class](https://docs.microsoft.com/en-us/dotnet/api/system.threading.tasks.task?view=net-6.0) is roughly equivalent to a [JavaScript Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global\_Objects/Promise).

A Task:

* encapsulates logic which will complete at some point
* tracks the completion state of the logic

On the subject of Tasks and threading, the [Microsoft documentation](https://docs.microsoft.com/en-us/dotnet/standard/parallel-programming/task-based-asynchronous-programming) states:

> The Task Parallel Library (TPL) is based on the concept of a task, which represents an asynchronous operation. In some ways, a task resembles a thread or ThreadPool work item, but at a higher level of abstraction. The term task parallelism refers to one or more independent tasks running concurrently. Tasks provide two primary benefits:
>
> * More efficient and more scalable use of system resources.
> * More programmatic control than is possible with a thread or work item.
>
> For both of these reasons, TPL is the preferred API for writing multi-threaded, asynchronous, and parallel code in .NET.

It is important to note that Tasks help with multithreading, _but are not exclusive to multithreading_. As well as supporting parallelisation, Tasks can abstract other asychronous operations like single-threaded input/output - network requests, disk access or awaiting user input.

## Async and Await

**Watch the following two videos** which explain the basics of task-based programming in C#

* 📹 [https://www.youtube.com/watch?v=X9N5r6kMOxw](https://www.youtube.com/watch?v=X9N5r6kMOxw)
* 📹 [https://www.youtube.com/watch?v=cVtTsaNQQ1M](https://www.youtube.com/watch?v=cVtTsaNQQ1M)

## Interactive Exercises

To prove what we've just learned, [**Open this notebook**](vscode://ms-dotnettools.dotnet-interactive-vscode/openNotebook?notebookFormat=dib\&url=https://andigital.gitlab.io/hamilton/learning-dotnet/csharp\_fundamentals/async.dib) and work through the exercises.
