# Types

## Interactive Module

This module relies on .NET Interactive Notebooks.

[Open this notebook](vscode://ms-dotnettools.dotnet-interactive-vscode/openNotebook?notebookFormat=dib\&url=https://andigital.gitlab.io/hamilton/learning-dotnet/csharp\_fundamentals/csharp\_types.dib) and work through the examples.
