# Extension methods

## Extension methods

Consider an `EventTicket` class provided by a 3rd party ticketing system. This class and its assembly are controlled by someone else; we have no ability to modify it directly. If the class is `sealed` we cannot even extend it ourselves by creating a derived type.

How do we add behaviour to this `EventTicket` class?

From the **Microsoft documentation**:

> Extension methods enable you to "add" methods to existing types without creating a new derived type, recompiling, or otherwise modifying the original type. Extension methods are defined as static methods but are called by using instance method syntax.

## Static methods

What are static methods? Let's recap.

We know that we can create instances of classes, and these instances can have properties and methods.

Classes can also have `static` properties and methods, which are associated with the class its self _and not the instance of the class_. For example, `int.Parse` is a static method on `int` that accepts a string and converts it to an integer.

Other examples of static methods and properties include:

* `Program.Main` method, a traditional entry point for any executable application
* `WebApplication.CreateBuilder` method, returns a new instance of a WebApplication builder with common default values applied.

## Writing extension methods

**Work through the** [**.Net Notebook**](vscode://ms-dotnettools.dotnet-interactive-vscode/openNotebook?notebookFormat=dib\&url=https://andigital.gitlab.io/hamilton/learning-dotnet/csharp\_fundamentals/extension\_methods.dib) **to learn how extension methods are created and applied**
