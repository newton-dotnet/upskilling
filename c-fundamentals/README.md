# C# fundamentals

## Overview

C# is a strongly typed language.

The type system comprises of primitives (simple data) and reference types (complex data). The differences between primitives and reference types impact usage, performance and computation.

**This section is interactive**

We use .NET Interactive Notebooks to provide executable examples alongside learning material. Ensure you have the [.NET Interactive Notebooks plugin](https://marketplace.visualstudio.com/items?itemName=ms-dotnettools.dotnet-interactive-vscode) installed.

Read more about [interactive notebooks here](https://github.com/dotnet/interactive).

## What you will learn

* How to declare and convert types from one to another
* How to model your logic with Object Oriented principles
* How to work with collections
* How to recover from errors
* Advanced techniques for clean code using extension methods

## Topics

* [Types](types.md)
* [Classes](classes.md)
* [Interfaces](interfaces.md)
* [IEnumerable and LINQ](ienumerable-and-linq.md)
* [Asynchronous operations](async-with-tasks.md)
* [Exceptions and error handling](exceptions-and-error-handling.md)
* [Extension methods](extension-methods.md)
