# IEnumerable and LINQ

## About LINQ

Language-Integrated Query (LINQ) is a feature of C# which allows you to write queries over collections in plain language.

**Watch the following three videos to see LINQ in action**

* 📹 [Part 1](https://www.youtube.com/watch?v=p5myHVOtmiU)
* 📹 [Part 2](https://www.youtube.com/watch?v=uUsnDXYRADA)
* 📹 [Part 3](https://www.youtube.com/watch?v=gFkRzY-zwzc)

These three videos show you the basics of LINQ syntax. If you want to dive deeper, feel free to use `dotnet try` as described in the video.

To recap:

1. LINQ can be written in a language similar to SQL, for example `from item in collection select item.Property`
2. LINQ can also be written using methods, for example `collection.Select(item => item.Property)`
3. LINQ has the concept of 'eager' and 'lazy' resolution. Eager resolution can be forced by writing `.ToList()`

On that last point, let's dig deeper.

## IEnumerable

LINQ is built upon the basic concept of IEnumerable.

`IEnumerable` is a simple interface that describes _things which can be enumerated_. Let's work through some examples to demonstrate.

**Open** [**this notebook**](vscode://ms-dotnettools.dotnet-interactive-vscode/openNotebook?notebookFormat=dib\&url=https://andigital.gitlab.io/hamilton/learning-dotnet/csharp\_fundamentals/ienumerable.dib) **and work through the examples**

## LINQ methods

JavaScript provides basic array processing methods like `filter`, `map`, `sort` or `reduce`. LINQ provides many many more. Take some time to familiarise yourself with some of the LINQ methods at the [linqsamples.com site](https://linqsamples.com/) and experiment with your own code.

**Here are two challenges to apply your knowledge of LINQ**

Use either the .NET interactive notebook, or `dotnet new console` to create an application for this code.

### Challenge: Sort a sentence

Start with this snippet of text:

```csharp
var source = @"Rising up, back on the street
Did my time, took my chances
Went the distance, now I'm back on my feet
Just a man and his will to survive";
```

Write some code which splits the sentence into separate words, then sorts those words alphabetically before printing out the result.

You may find the string [Replace](https://docs.microsoft.com/en-us/dotnet/api/system.string.replace?view=net-6.0) and [Concat](https://docs.microsoft.com/en-us/dotnet/api/system.string.concat?view=net-6.0) methods useful.

### Challenge: Find duplicates

Given this list of numbers:

```
var number = new List<int> { 1, 1, 2, 9, 3, 5, 5, 0, 6, 0 };
```

Write some LINQ to identify which numbers appear more than once in this collection in numerical order.

For example, we expect this to display `0, 1, 5`.
